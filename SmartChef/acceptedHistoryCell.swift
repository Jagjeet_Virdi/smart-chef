//
//  acceptedHistoryCell.swift
//  SmartChef
//
//  Created by Mac Solutions on 30/03/18.
//  Copyright © 2018 osx. All rights reserved.
//

import UIKit

class acceptedHistoryCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var designImage: UIImageView!
    @IBOutlet weak var designLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
