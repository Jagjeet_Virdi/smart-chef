//
//  AppDelegate.swift
//  SmartChef
//
//  Created by osx on 26/08/17.
//  Copyright © 2017 osx. All rights reserved.
// //com.exousia.GMapsDemo   com.codeApp.smartChef

import UIKit
import GoogleMaps
import GooglePlaces
import Google
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import StoreKit
import CoreLocation
import IQKeyboardManagerSwift
import TwitterKit
import SendBirdSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate {
   
    var AppUserDefaults = UserDefaults.standard
    var window: UIWindow?
    var locationManager = CLLocationManager()
  
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
      IQKeyboardManager.shared.enable = true
      SBDMain.initWithApplicationId("7235944B-A3EA-4923-B90F-DC268DB772F5")

      if (CLLocationManager.locationServicesEnabled()) {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
      } else {
        print("Location services are not enabled");
      }
      LocationStore.sharedInstance.isLocationTypeCurrent = true
      
        GMSServices.provideAPIKey("AIzaSyDg8lsK9V2UmK3KV5pFrhSpMHQ2YiOWqqQ")
        
        // *** For Places ***************
        GMSPlacesClient.provideAPIKey("AIzaSyDg8lsK9V2UmK3KV5pFrhSpMHQ2YiOWqqQ")
        
        PayPalMobile .initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "Ab_EA6lpQXdn5lo6M8N2XlwSqUR-m1v4PXX6oK5yVGkyOteFr2KZHMagGo0WdXdmMRGPJRz16HrDqDri",PayPalEnvironmentSandbox: "hadi_jorjani-facilitator@yahoo.com"])

      TWTRTwitter.sharedInstance().start(withConsumerKey:"dTIFEvKTYAFK1T2gcqxo125ZT", consumerSecret:"fVMA5vnQkqXV3ItlvNkjdJFB18L1B6RgnRp5wJwC6V9yS1I5Ui")

         return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    let locationArray = locations as NSArray
    let locationObj = locationArray.lastObject as! CLLocation
    let coord = locationObj.coordinate
    LocationStore.sharedInstance.latitude = String(describing: coord.latitude)
    LocationStore.sharedInstance.longitude = String(describing: coord.longitude)
  }
  
  
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func requestReview(){
         SKStoreReviewController.requestReview()
    }

    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        FBSDKAppEvents.activateApp()

        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        AppUserDefaults.set(deviceTokenString, forKey: "deviceToken")
        print("APNs device token: \(deviceTokenString)")
    }
    
}

