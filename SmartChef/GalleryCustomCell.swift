//
//  GalleryCustomCell.swift
//  SmartChef
//
//  Created by Jagjeet Singh on 03/05/18.
//  Copyright © 2018 osx. All rights reserved.
//

import UIKit

class GalleryCustomCell: UICollectionViewCell {
    
  @IBOutlet var imageView: UIImageView!
}
