//
//  Snacks_Item_Cell.swift
//  SmartChef
//
//  Created by osx on 26/08/17.
//  Copyright © 2017 osx. All rights reserved.
//

import UIKit

class Snacks_Item_Cell: UICollectionViewCell {
   
    @IBOutlet weak var Snack_Item_Image: UIImageView!
    
    @IBOutlet weak var Snack_Item_Label: UILabel!
    
    
    
}
