//
//  Setting_Inside_Cell.swift
//  SmartChef
//
//  Created by osx on 29/08/17.
//  Copyright © 2017 osx. All rights reserved.
//

import UIKit

class Setting_Inside_Cell: UITableViewCell {

    // **** Outlets ********
    
    @IBOutlet weak var Setting_Inside_Label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
