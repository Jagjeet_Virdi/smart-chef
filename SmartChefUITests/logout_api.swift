//
//  logout_api.swift
//  SmartChef
//
//  Created by Mac Solutions on 27/12/17.
//  Copyright © 2017 osx. All rights reserved.
//

import UIKit

class logout_api: NSObject {

    func logout(authorization: String,sessionTime: String,userId : String, completion: @escaping (_ success: Bool) -> Void){
        // ****** Remove Arrays *******
        
        let Parameters = ["authorization":"\(authorization)","sessionTime":"\(sessionTime)","userId":"\(userId)"] as [String: String]
        
        print("PARAMETERS Of LOGOUT :\(Parameters)")
        
        let URL_Constant = URLConstants().BASE_URL + URLConstants().LOGOUT
        
        WebService.requestPostUrl(URL_Constant, parameters: Parameters, success: { (JSONResponse) -> Void in
            
            
            print("JSONResponse of LOGOUT \(JSONResponse)")
            let response = JSONResponse["response"].dictionaryValue
            print("Response of LOGOUT \(response)")
            // ****** Status **************************
            
            let status = JSONResponse["status"].stringValue
            
            if status == "1"{
                
                completion(true)
                
            }else{
                //   SwiftLoader.hide()
                completion(false)
                
            }
            
            
        }, failure: { (error) -> Void in
            print("error in Qabeli_Type = \(error.localizedDescription)")
        })
    }
    
    
    func forgetPass(email : String, completion: @escaping (_ success: Bool) -> Void){
        // ****** Remove Arrays *******
        
        let Parameters = ["email":"\(email)"] as [String: String]
        
        print("PARAMETERS Of FORGOTPASS :\(Parameters)")
        
        let URL_Constant = URLConstants().BASE_URL + URLConstants().FORGOT_PASS
        
        WebService.requestPostUrl(URL_Constant, parameters: Parameters, success: { (JSONResponse) -> Void in
            
            
            print("JSONResponse of FORGOTPASS \(JSONResponse)")
            let response = JSONResponse["response"].dictionaryValue
            print("Response of FORGOTPASS \(response)")
            // ****** Status **************************
            
            let status = JSONResponse["status"].stringValue
            
            if status == "1"{
                
                completion(true)
                
            }else{
                completion(false)
            }
        }, failure: { (error) -> Void in
            print("error in Qabeli_Type = \(error.localizedDescription)")
        })
    }
}
