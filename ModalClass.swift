//
//  ModalClass.swift
//  ApplePay
//
//  Created by MAC on 01/12/17.
//  Copyright © 2017 codeApp. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ModalClass: NSObject {
    var username  : String = ""
    var Authorization : String = ""
    var sessionTime : String = ""
    var userId: String = ""
    class var singleton:ModalClass
    {
        struct sharedInstance
        {
            static let instance = ModalClass()
        }
        return sharedInstance.instance
    }
    
    func setUserInfo(_ data : JSON){
        username = ""
        Authorization = ""
        sessionTime = ""
        userId = ""
        if let string_username = data["username"].string {
            username = string_username
        }
        if let string_Auth = data["Authorization"].string {
            Authorization = string_Auth
        }
        if let string_session = data["sessionTime"].string {
            sessionTime = string_session
        }
        if let string_userId = data["userId"].string {
            userId = string_userId
        }
    }
}

