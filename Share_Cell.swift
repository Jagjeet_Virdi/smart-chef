//
//  Share_Cell.swift
//  SmartChef
//
//  Created by osx on 01/09/17.
//  Copyright © 2017 osx. All rights reserved.
//

import UIKit

class Share_Cell: UITableViewCell {

    // ****** Outlets ******
    
    @IBOutlet weak var Share_Label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
